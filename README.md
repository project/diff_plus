# Diff Plus

This module provides opinionated extensions to the diff module that
can be developed with a higher velocity than the base module.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/diff_plus).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/diff_plus).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Diff](https://www.drupal.org/project/diff)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

This module takes a user-centric approach by optionally allowing for per-user
customization of most configuration items. Site owners may (and are encouraged
to) grant roles the permission to customize diff plus configurations. Since not
everyone is identical, users should be able to optimize their own editorial
experience.


## Maintainers

- Luke Leber - [Luke.Leber](https://www.drupal.org/u/lukeleber)
